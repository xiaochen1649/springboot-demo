FROM mcr.microsoft.com/devcontainers/java:dev-8

LABEL maintainer="wenzhihuai@globalegrow.com"


WORKDIR /data/work

RUN cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

# ADD target/demo-0.0.1-SNAPSHOT.jar /data/work/demo-0.0.1-SNAPSHOT.jar

# CMD ["java","-jar","demo-0.0.1-SNAPSHOT.jar"]

# EXPOSE 8080